import { expect } from 'chai';
import { testCases } from './testData';
import { sortCategoriesForInsert } from '../src/index';

testCases.forEach(({ input, output }) => 
    console.log(sortCategoriesForInsert(input))
);

describe('sortCategoriesForInsert()', function() {
    it('should sort input properly and match output definition', function() {
        testCases.forEach(({ input, output }) => 
            expect(sortCategoriesForInsert(input))
                .to.deep.equal(output)
        );
    })
})