export const testCases = [
  {
    // 1 singular hierarchy
    input: [
      {
        name: "Accessories",
        id: 1,
        parent_id: 20,
      },
      {
        name: "Watches",
        id: 57,
        parent_id: 1,
      },
      {
        name: "Men",
        id: 20,
        parent_id: null,
      },
    ],
    output: [
      {
        name: "Men",
        id: 20,
        parent_id: null,
      },
      {
        name: "Accessories",
        id: 1,
        parent_id: 20,
      },
      {
        name: "Watches",
        id: 57,
        parent_id: 1,
      },
    ],
  },
  {
    // several hierarchies
    input: [
      {
        name: "Accessories",
        id: 1,
        parent_id: 20,
      },
      {
        name: "Living Room",
        id: 24,
        parent_id: 8,
      },
      {
        name: "Kitchen",
        id: 14,
        parent_id: null,
      },
      {
        name: "Watches",
        id: 57,
        parent_id: 1,
      },
      {
        name: "Furniture",
        id: 8,
        parent_id: null,
      },
      {
        name: "Men",
        id: 20,
        parent_id: null,
      },
      {
        name: "Baking",
        id: 65,
        parent_id: 14,
      },
      {
        name: "Couches",
        id: 37,
        parent_id: 24,
      },
      {
        name: "Stoves",
        id: 87,
        parent_id: 65,
      },
    ],
    output: [
      {
        name: "Kitchen",
        id: 14,
        parent_id: null,
      },
      {
        name: "Baking",
        id: 65,
        parent_id: 14,
      },
      {
        name: "Stoves",
        id: 87,
        parent_id: 65,
      },
      {
        name: "Furniture",
        id: 8,
        parent_id: null,
      },
      {
        name: "Living Room",
        id: 24,
        parent_id: 8,
      },
      {
        name: "Couches",
        id: 37,
        parent_id: 24,
      },
      {
        name: "Men",
        id: 20,
        parent_id: null,
      },
      {
        name: "Accessories",
        id: 1,
        parent_id: 20,
      },
      {
        name: "Watches",
        id: 57,
        parent_id: 1,
      },
    ],
  },
];
