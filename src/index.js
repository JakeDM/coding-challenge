export function sortCategoriesForInsert(inputJson) {
  return hierarchySort(buildLookupTable(inputJson), null, []);
}

// Key value lookup of parent ids to categories
function buildLookupTable(inputArray) {
  return inputArray.reduce((lookupTable, value) => {
    if (!lookupTable[value.parent_id]) {
      lookupTable[value.parent_id] = [];
    }
    lookupTable[value.parent_id].push(value);
    return lookupTable;
  }, {});
}

// Recursively iterates over the lookup table to place categories below their parent
function hierarchySort(lookupTable, parentId, result) {
  if (!lookupTable[parentId]) {
      return;
  }

  const currentCategories = lookupTable[parentId];
  for (let i = 0; i < currentCategories.length; i++) {
    result.push(currentCategories[i]);
    hierarchySort(lookupTable, currentCategories[i].id, result);
  }

  return result;
}
