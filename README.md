# Jacob Miller's Coding Challenge Entry
The file containing all of the business logic is `src/index.js`.

run `npm i` and run `npm test` to run my test cases. The test cases contain 2 examples; one example being the example provided in the description of the problem and another example with several category hierarchies. See `test/testData.js` to see the inputs and expected outputs.

**NOTE:** You need node v12+ for the tests to run as it's necessary for babel runtime to work with mocha.